var express = require('express');
var router = express.Router();

//////
var passport = require('passport');
var flash = require('connect-flash');
/////

var mysql      = require('mysql');
var connection = mysql.createConnection({
  host     : 'localhost',
  user     : 'root',
  password : '',
  database : 'bazalot'
});

connection.connect();

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { 
    title: 'System samolotowy',
    user: req.user
  });
});

router.get('/admin', function(req, res, next) {
  res.render('admin');
});

router.get('/dodajLot', function(req, res, next) {
  res.render('dodajLot');
});

router.post('/dodajLot', function(req, res, next) {
  //var nowyLot = { id: req.body.id, nazwaSamolotu: req.id.nazwaSamolotu, miejsceWylotu: req.id.miejsceWylotu, miejscePrzylotu: req.id.miejscePrzylotu, dataWylotu: req.id.dataWylotu, godzinaWylotu: req.id.godzinaWylotu, godzinaPrzylotu: req.id.godzinaPrzylotu, cena: req.id.cena };
    connection.query('INSERT INTO `lot`(`id`, `nazwaSamolotu`, `miejsceWylotu`, `miejscePrzylotu`, `dataWylotu`, `godzinaWylotu`, `godzinaPrzylotu`, `cena`) VALUES ('+req.body.id+', "'+req.body.nazwaSamolotu+'", "'+ req.body.miejsceWylotu+'", "'+req.body.miejscePrzylotu+'", "'+ req.body.dataWylotu+'", "'+req.body.godzinaWylotu+'", "'+ req.body.godzinaPrzylotu+'", '+req.body.cena+')', function(err, rows, fields) {
       if (err) throw err;
     });
    /*
    console.log(nowyLot);
    connection.query('INSERT INTO lot SET ?', nowyLot, function(err, res){
      if (err) throw err;
    });
    */
console.log(req.body);
});

router.get('/getDaneWykresu',function(req,res){
  connection.query('SELECT l.miejsceWylotu, COUNT(l.id) as liczbaMiast FROM Lot l GROUP BY l.miejsceWylotu', function(err,rows,fields){
    if (err) throw err;
    var miasta=[];
    var iloscLotow=[];
    for(var i=0;i<rows.length;i++){
      if(miejsceWylotu.indexOf(rows[i].miejsceWylotu)==-1){
        miejsceWylotu.push(rows[i].miejsceWylotu);
      }
      if(liczbaMiast.indexOf(rows[i].liczbaMiast)==-1){
        liczbaMiast.push(rows[i].liczbaMiast);
      }
    }
    res.json({miejsceWylotu: miejsceWylotu, liczbaMiast: liczbaMiast});
  });
});

router.get('/getMiastaPrzylotu',function(req, res){
 connection.query('SELECT  l.miejsceWylotu, l.miejscePrzylotu  FROM Lot l ', function(err, rows, fields) {
    if (err) throw err;
    var miejsceWylotu=[];
    var miejscePrzylotu=[];
for(var i=0;i<rows.length;i++){
if(miejsceWylotu.indexOf(rows[i].miejsceWylotu)==-1){
  miejsceWylotu.push(rows[i].miejsceWylotu);
}

if(miejscePrzylotu.indexOf(rows[i].miejscePrzylotu)==-1){
  miejscePrzylotu.push(rows[i].miejscePrzylotu);
}
}
    res.json({miejsceWylotu: miejsceWylotu, miejscePrzylotu: miejscePrzylotu});
  });
});

router.post('/dajLoty', function(req, res, next) {

  connection.query('SELECT * FROM Lot WHERE miejsceWylotu = \''+req.body.miejsceWylotu+'\' and miejscePrzylotu = \''+req.body.miejscePrzylotu+'\' and dataWylotu = \''+req.body.dataWylotu+'\' AND miejscePrzylotu != miejsceWylotu', function(err, rows, fields) {
    if (err) throw err;

    res.json({data: rows});
  });
});

router.get('/dajBilety', function(req, res, next) {

  connection.query('SELECT b.id as idBiletu, l.miejscePrzylotu as miejscePrzylotu, l.miejsceWylotu as miejsceWylotu, b.koszt FROM Bilet as b JOIN Lot as l ON b.idLotu = l.id WHERE idKlienta = 23 and czyZarezerwowano = false', function(err, rows, fields) {
    if (err) throw err;

    res.json({data: rows});
  });
});

router.get('/dajRezerwacje', function(req, res, next) {

  connection.query('SELECT r.idRezerwacji, r.idBiletu, r.dataRezerwacji, r.liczbaDni, b.koszt, l.miejsceWylotu, l.miejscePrzylotu  FROM Rezerwacja r JOIN Bilet b ON r.idBiletu = b.id JOIN Lot l ON b.idLotu = l.id ', function(err, rows, fields) {
    if (err) throw err;

    res.json({data: rows});
  });
});

router.post('/dodajDoKoszyka', function(req, res, next) {
  var liczbaBiletow = req.body.liczbaBiletow;
  console.log(liczbaBiletow);
  for (var i = 1;i<=liczbaBiletow;i++){
    connection.query('INSERT INTO `bilet`(`idLotu`, `koszt`, `nrMiejsca`, `idKlienta`) VALUES ('+req.body.wybranyLot.id+', '+ req.body.wybranyLot.cena+', '+i+', 23)', function(err, rows, fields) {
      if (err) throw err;
    });
  }
});

router.get('/wyszukaj', function(req, res, next) {
  res.render('wyszukaj');
});

router.get('/koszyk', function(req, res, next) {

    res.render('koszyk', {});

});

router.get('/rezerwacje', function(req, res, next) {
  
    res.render('rezerwacje', {});

});

router.post('/usunRezerwacje', function(req, res, next) {

  connection.query('DELETE FROM Rezerwacja WHERE idRezerwacji = '+req.body.idRezerwacji, function(err, rows, fields) {
    res.render('rezerwacje', {});
  });

});

router.post('/usunBilet', function(req, res, next) {

  connection.query('DELETE FROM Bilet WHERE id = '+req.body.idBiletu, function(err, rows, fields) {
    res.render('koszyk', {});
  });

});

router.post('/usunBiletZRezerwacji', function(req, res, next) {

  connection.query('DELETE FROM Rezerwacja WHERE idBiletu = '+req.body.idBiletu+' AND idRezerwacji = '+req.body.idRezerwacji, function(err, rows, fields) {
    res.render('koszyk', {});
  });

});

router.post('/edytujLiczbeDni', function(req, res, next) {

  connection.query('UPDATE Rezerwacja SET liczbaDni = '+req.body.liczbaDni+' WHERE idRezerwacji = '+req.body.idRezerwacji+';', function(err, rows, fields) {
    res.render('rezerwacje', {});
  });

});

router.post('/zarezerwuj', function(req, res, next) {
  connection.query('SELECT COALESCE(MAX(idRezerwacji),0) AS najwiekszaRezerwacja FROM Rezerwacja', function(err, najwyzsza, fields) {


    connection.query('SELECT b.id AS idBiletu FROM Bilet as b JOIN Lot as l ON b.idLotu = l.id WHERE idKlienta = 23 and czyZarezerwowano = false', function(err, rows, fields) {
      if (err) throw err;
      console.log(rows);
      for (var row in rows){
        connection.query('INSERT INTO Rezerwacja(idRezerwacji, idBiletu, dataRezerwacji, liczbaDni) VALUES ('+(najwyzsza[0].najwiekszaRezerwacja+1)+','+rows[row].idBiletu+', CURRENT_TIMESTAMP ,'+req.body.liczbaDni+')', function(err, rows, fields) {
        if (err) throw err;
        });
      }
      connection.query('UPDATE `bilet` SET `czyZarezerwowano`=true WHERE idKlienta = 23 and czyZarezerwowano = false', function(err, rows, fields) {
        if (err) throw err;
      });
    });

    });

});

router.get('/login', function(req, res) {
  res.render('login.ejs', {message: req.flash('loginMessage')});
});


router.post('/login', passport.authenticate('local-login', {
  successRedirect : '/profile',
  failureRedirect : '/login',
  failureFlash : true
    }),
  function(req, res){
    console.log("hello");
    if(req.body.remeber){
      req.session.cookie.maxAge = 1000 * 60 * 3;
    } else {
      req.session.cookie.expires = false;
    }
    res.redirect('/');
  }
);

router.get('/signup', function(req, res){
  res.render('signup.ejs', {message: req.flash('signupMessage')});
});

router.post('/signup', passport.authenticate('local-signup',{
  successRedirect : '/profile',
  failureRedirect : '/signup',
  failureFlash : true
}));

router.get('/profile', isLoggedIn, function(req, res){
  res.render('profile.ejs', {
    user : req.user
  });
});

router.get('/logout', function(req, res){
  req.logout();
  res.redirect('/');
});

function isLoggedIn(req, res, next){
  if(req.isAuthenticated())
    return next();
  res.redirect('/');
};
module.exports = router;
