app.controller('rezerwacjaController', ['$scope', '$http', '$window', '$log' , '$filter', function($scope, $http, $window, $log, $filter) {
	$scope.init = function(rows){
		$scope.mode = 'lista';
		$scope.rows = [];
		$scope.liczbaDni = 0;
		$scope.idRezerwacjiEdytowanej = 0;
		$http.get('/dajRezerwacje').success(function(data, status, headers, config) {
			$scope.rezerwacje = data.data;
			var temp = {};
			window._.each($scope.rezerwacje, function(rezerwacja){
				if (temp[rezerwacja.idRezerwacji] == null){
					temp[rezerwacja.idRezerwacji] = [];
				} 
				temp[rezerwacja.idRezerwacji].push(rezerwacja);
			});
			$scope.rezerwacje = temp;
			console.log($scope.rezerwacje);
	    }).error(function(data, status, headers, config) {
	    });
	};

	$scope.dajRezerwacje = function(data){
		return moment(data).format('DD/MM/YYYY');
	};

	$scope.usunRezerwacje= function(idRezerwacji){
		$http.post('/usunRezerwacje', {idRezerwacji: idRezerwacji}).success(function(data, status, headers, config) {
			delete $scope.rezerwacje[idRezerwacji];
	    }).error(function(data, status, headers, config) {
	    });
	};

	$scope.edytujRezerwacje= function(idRezerwacji, liczbaDni){
		$scope.mode = 'edycja';
		$scope.idRezerwacjiEdytowanej = idRezerwacji;
		$scope.liczbaDni = liczbaDni;
	};

	$scope.zmienLiczbeDni = function(){
		console.log('asdasd');
		$http.post('/edytujLiczbeDni', {liczbaDni: $scope.liczbaDni, idRezerwacji:$scope.idRezerwacjiEdytowanej}).success(function(data, status, headers, config) {
			$scope.mode = 'lista';
			window._.each($scope.rezerwacje[$scope.idRezerwacjiEdytowanej], function(rezerwacja, index){
				$scope.rezerwacje[$scope.idRezerwacjiEdytowanej][index].liczbaDni = $scope.liczbaDni;
			});
	    }).error(function(data, status, headers, config) {
	    });
	};

	$scope.usunBiletZRezerwacji= function(idRezerwacji, idBiletu){
		$http.post('/usunBiletZRezerwacji', {idRezerwacji: idRezerwacji, idBiletu:idBiletu}).success(function(data, status, headers, config) {

			$scope.rezerwacje[idRezerwacji] = window._.filter($scope.rezerwacje[idRezerwacji], function(rezerwacja){
				return rezerwacja.idBiletu != idBiletu;
			});
	    }).error(function(data, status, headers, config) {
	    });
	};


	$scope.zaplac = function(){
		$scope.mode = 'zaplac';
	};



}]);