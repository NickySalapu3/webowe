app.controller('wyszukajController', ['$scope', '$http', '$window', '$log' , '$filter', function($scope, $http, $window, $log, $filter) {
	$scope.init = function(){
		$http.get('/getMiastaPrzylotu').success(function(data, status, headers, config) {
		$scope.miejscaWylotu = data.miejsceWylotu;
		$scope.wybrane.miejsceWylotu = $scope.miejscaWylotu[0];
		$scope.miejscaPrzylotu = data.miejscePrzylotu;
		$scope.wybrane.miejscePrzylotu = $scope.miejscaPrzylotu[0];
	    }).error(function(data, status, headers, config) {
	    });
		$scope.wybrane = {};
		$scope.mode = 'wyszukaj';
		$scope.loty = [];
		$scope.wybranyLot = {};
		$scope.liczbaBiletow = 1;
		$scope.slider = {};
		$scope.slider.options = {
			ceil: 100
		};
	};

	

	$scope.wyszukaj = function(){
		$scope.mode = 'lista';

		$scope.wybrane.dataWylotu = moment($scope.dataWylotu).format('DD/MM/YYYY');

		$http.post('/dajLoty', $scope.wybrane).success(function(data, status, headers, config) {
			console.log(data);
	        $scope.loty = data.data;
	        console.log($scope.loty);
	    }).error(function(data, status, headers, config) {
	    });
	};

	$scope.wyswietl = function(lot){
		$scope.mode = 'wyswietl';
		$scope.wybranyLot = lot;
	};

	$scope.dodajDoKoszyka = function(){
		$scope.mode = 'poDodaniu';
		$http.post('/dodajDoKoszyka', {wybranyLot: $scope.wybranyLot, liczbaBiletow: $scope.liczbaBiletow}).success(function(data, status, headers, config) {
			
	    }).error(function(data, status, headers, config) {
	    });
	};

	$scope.wyslijDane = function(){
		$http.post('/dodajLot', $scope.dane).success(function(data, status, headers, config) {
			
	    }).error(function(data, status, headers, config) {
	    });
	}

}]);