var app = angular.module('myApp', [
  'mgcrea.ngStrap',
  'datePicker',
  'ngAnimate',
  'ngAria',
  'ngMaterial',
  'rzModule'
]);

app.config(function($interpolateProvider){
    $interpolateProvider.startSymbol('{[{').endSymbol('}]}');
});

app.controller('mainController', ['$scope', '$http', '$window', '$log' , '$filter', function($scope, $http, $window, $log, $filter) {

}]);