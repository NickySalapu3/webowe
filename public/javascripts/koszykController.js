app.controller('koszykController', ['$scope', '$http', '$window', '$log' , '$filter', function($scope, $http, $window, $log, $filter) {
	$scope.init = function(rows){
		$scope.liczbaDni = 0;
		$scope.mode = 'lista';
		$scope.rows = [];
		$http.get('/dajBilety').success(function(data, status, headers, config) {
			$scope.rows = data.data;
	    }).error(function(data, status, headers, config) {
	    });
	};

	$scope.zarezerwuj = function(){
		$scope.mode = 'zarezerwowano';
		$http.post('/zarezerwuj', {liczbaDni: $scope.liczbaDni}).success(function(data, status, headers, config) {
	    }).error(function(data, status, headers, config) {
	    });
	};

	$scope.usunBilet= function(id){
		$http.post('/usunBilet', {idBiletu: id}).success(function(data, status, headers, config) {
			$scope.rows = $scope.rows.filter(function (el) {
				  return el.idBiletu !== id;
			 });
	    }).error(function(data, status, headers, config) {
	    });
	};

}]);