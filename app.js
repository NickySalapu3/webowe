var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
///////

var passport = require('passport');
var flash = require('connect-flash');
var morgan = require('morgan');
var session = require('express-session');

require('./config/passport')(passport);

///////


var routes = require('./routes/index');
var users = require('./routes/users');

var app = express();

var hbs = require('hbs');

hbs.registerHelper('__', function(){
  return i18n.__.apply(this, arguments);
});

hbs.registerHelper('__n', function(){
  return i18n.__n.apply(this, arguments);
});

var ejs = require('ejs');

/*
  app.helpers({
  '__': i18n.__,
  '__n': i18n.__n
});
*/

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'hbs');

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use(session({secret: '{secret}', name: 'session_id', saveUninitialized: true, resave: true}));
app.use(passport.initialize());
app.use(passport.session());
app.use(flash());
app.use(function(req, res, next){
  res.locals.user = req.user;
  next();
});

var i18n = require('i18n');

i18n.configure({
  locales: ['pl', 'en', 'de'],
  cookie: 'locale',
  directory: "" + __dirname + "/locales"
});

app.use(i18n.init);

app.use('/node_modules',  express.static(__dirname + '/node_modules'));

app.use('/', routes);
app.use('/users', users);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
  app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
      message: err.message,
      error: err
    });
  });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
  res.status(err.status || 500);
  res.render('error', {
    message: err.message,
    error: {}
  });
});


module.exports = app;
