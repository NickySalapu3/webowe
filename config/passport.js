var LocalStrategy = require('passport-local').Strategy;

var mysql = require('mysql');
var bcrypt = require('bcrypt-nodejs');
//var dbconfig = require('./database');
//var connection = mysql.createConnection(dbconfig.connection);

var connection = mysql.createConnection({
  host     : 'localhost',
  user     : 'root',
  password : '',
  database : 'bazalot'
});

//connection.query('USE ' + dbconfig.database);
module.exports = function(passport){

	passport.serializeUser(function(user, done){
		done(null, user);
	});

	passport.deserializeUser(function(user, done){
		done(null, user);
	});

	passport.use(
		'local-signup',
		new LocalStrategy({
			usernameField : 'username',
			passwordField : 'password',
			passReqToCallback : true
		},
		function(req, username, password, done){
			connection.query("SELECT * FROM users WHERE username = ?",[username],function(err, rows){
				console.log(rows);
				console.log("above row object");
				if (err)
					return done(err);
				if (rows.length){
					return done(null, false, req.flash('signupMessage', 'Nazwa uzytkownika zajeta.'));
				} else {
					var newUserMysql = {
						username: username,
						password: bcrypt.hashSync(password, null, null)
					};

					var insertQuery = "INSERT INTO users ( username, password ) values (?,?)";
					console.log(insertQuery);
					connection.query(insertQuery,[newUserMysql.username, newUserMysql.password], function(err, rows){
						newUserMysql.id = rows.insertId;
						return done(null, newUserMysql);
					});
				}
			});
		})
	);

	passport.use(
		'local-login',
		new LocalStrategy({
			usernameField : 'username',
			passwordField : 'password',
			passReqToCallback : true
		},
		function(req, username, password, done){
			connection.query("SELECT * FROM users WHERE username = ?",[username], function(err, rows){
				if (err)
					return done(err);
				if (!rows.length){
					return done(null, false, req.flash('loginMessage', 'Nie znaleziono uzytkownika.'));
				}

				if(!bcrypt.compareSync(password, rows[0].password))
					return done(null, false, req.flash('loginMessage', 'Bledne haslo.'));

				return done(null, rows[0]);
			});
		})
	);
};